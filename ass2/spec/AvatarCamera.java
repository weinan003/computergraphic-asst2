package ass2.spec;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import com.sun.org.apache.regexp.internal.recompile;

public class AvatarCamera implements CameraInterface{
	private static final int FIRST = 1;
	private static final int THIRD = 3;
	private double cX;
    private double cY;
    private double cZ;
    public double alpha;
    public double beta;
    private double myRadius;
    private double floatingHeight = 1;
	private int cameraMode;
	private float exactLoc[] = new float[3];
    public AvatarCamera(double x,double y, double z) {
       myRadius = 1;
       cX = x;
       cY = y + floatingHeight;
       cZ = z;
       alpha = 0.0;
       beta = 0.0;
    }
    

    @Override
	public void reshape(GL2 gl, int width, int height) {
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        double ratio = 1.0 * width / height;
        GLU glu = new GLU();
        glu.gluPerspective(45, ratio, 0.001, 200);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }
    
    /**
     * if the third person view, then move the camera back, however the camera will point
     * to avatar, therefore always tracking avator's movement like going up and down hills
     * 
     * if first person view then u can look up and down.
     */
    @Override
    public void Draw() {
    	GLU glu = new GLU();
    	 double backX = 0;
         double backZ = 0;
         double backY = 0;
    	if (cameraMode == FIRST) {
    		
    		double y = cY + Math.sin(beta)* myRadius;
    		double zxRadius = Math.cos(beta)* myRadius;
    		
    		
    		double x = cX + Math.cos(alpha) * zxRadius;
            double z = cZ + Math.sin(alpha) * myRadius;
            
            backX =  cX - Math.cos(alpha) * zxRadius;
            backZ =  cZ - Math.sin(alpha) * zxRadius;
            backY = cY;
            glu.gluLookAt(backX, backY , backZ, x, y, z, 0, 1, 0);
    	} else {
    		backX =  cX - Math.cos(alpha) * myRadius;
            backZ =  cZ - Math.sin(alpha) * myRadius;
            backY =  Terrain.globalAltitude(backX, backZ) + floatingHeight + 0.3;
            
    		
    		glu.gluLookAt(backX, backY , backZ, cX, cY - 0.3, cZ, 0, 1, 0);
    	}
    	updateExactLoc(backX, backY, backZ);
    }

    private void updateExactLoc(double x, double y, double z) {
    	exactLoc[0] = (float) x;
    	exactLoc[1] = (float) y;
    	exactLoc[2] = (float) z;
    }
    
    @Override
	public float[] getExactLoc() {
		return exactLoc;
		
	}

	public void setPosition(double x, double y, double z) {
		cX = x;
		cY = y + floatingHeight;
		cZ = z;
		
	}

	public void turnOnFirstMode() {
		cameraMode = FIRST;
		myRadius = 1;
	}


	public void turnOnThirdMode() {
		cameraMode = THIRD;
		myRadius = 1.5;
		
	}

}
