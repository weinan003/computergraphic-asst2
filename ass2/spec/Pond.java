package ass2.spec;

import java.awt.Point;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

/**
 * uses surface Revolution to draw the pond
 * @author JCBSH
 *
 */
public class Pond {
	private MyTexture myTexture;
    
	private double timeCount = 0;
	private int pointsPerCycle = 40;
	private int numOfcycle = 5;
	private double radius = 0.8;
	private double alpha = radius / (numOfcycle * Math.PI * 2.0);
	
	private double waterLevel = 0.2;
	private double rippleAmp = waterLevel/4;
	private double wallHeight =  0.2;
	private double wallWidth = 0.05;
	private double  wallBaseHeight = 0.01;
	private double  floorBaseHeight = 0.02;

    

    private double[] myPos;
    public Pond(double x, double y, double z) {
        myPos = new double[3];
        myPos[0] = x;
        myPos[1] = y;
        myPos[2] = z;
    }
    
	public void Draw(GL2 gl) {
		
        gl.glPushMatrix();
        //translate the coord frame to its location
        gl.glTranslated(myPos[0], myPos[1], myPos[2]);
        
        double[][] ripplePair = makeRipple();
        double[][] pondPair = makePond();

        setupWallMaterial(gl);
        drawRevo(pondPair[0], pondPair[1], gl);
        
        drawBase(gl);
        //alpha blend the water 
       	gl.glBlendFunc(GL2.GL_SRC_ALPHA,GL2.GL_ONE_MINUS_SRC_ALPHA);
       	gl.glEnable(GL2.GL_BLEND);
       	
       	
       	
        setupWaterMaterial(gl);
        drawRevo(ripplePair[0], ripplePair[1], gl);
        gl.glDisable(GL2.GL_BLEND);
        gl.glPopMatrix();	
	}


	/**
	 * draws a circle as the floor of the pond
	 * @param gl
	 */
    private void drawBase(GL2 gl) {
    	setupFloorMaterial(gl);
    	gl.glBegin(GL2.GL_TRIANGLE_FAN);
        {
        	int theta = 0;
            while(theta <= 360) {
            	double radians = Math.toRadians(theta);
            	gl.glVertex3d(radius*Math.cos(radians), floorBaseHeight, radius*Math.sin(radians));
            	theta ++;
            }
        }
        gl.glEnd();
		
	}

	public void normalize(double v[])  
    {  
        double d = Math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);  
        if (d != 0.0) 
        {  
           v[0]/=d; 
           v[1]/=d;  
           v[2]/=d;  
        }  
    } 
    
    void normCrossProd(double v1[], double v2[], double out[])  
    {  
       out[0] = v1[1]*v2[2] - v1[2]*v2[1];  
       out[1] = v1[2]*v2[0] - v1[0]*v2[2];  
       out[2] = v1[0]*v2[1] - v1[1]*v2[0];  
       normalize(out);  
    } 
    
    double [] makeProfileNormals(double[] r, double []y){
    	double [] n = new double[r.length*2];
    	double[] v0 = new double[2];
    	double[] v1 = new double[2];
    	for(int i = 0; i < r.length; i++){
    	    if (i == 0) {
    			v0[0] = r[i];
    			v0[1] = y[i];
    			v1[0] = r[i+1];
    			v1[1] = y[i+1];
    		
    		} else if (i == r.length-1) {
    			v0[0] = r[i-1];
    			v0[1] = y[i-1];
    			v1[0] = r[i];
    			v1[1] = y[i];
    		} else {
    			v0[0] = r[i-1];
    			v0[1] = y[i-1];
    			v1[0] = r[i+1];
    			v1[1] = y[i+1];
    		}
    		
    		double dx = v1[0] - v0[0];
    		double dy = v1[1] - v0[1];
    		double mag = Math.sqrt(dx*dx + dy*dy);
    	    if(mag != 0){
    	   
    		   dx = dx/mag;
    		   dy = dy/mag;
    	    }
    		//create a normal from the tangent in 2D
    		n[i*2] = dy; 
    		n[i*2+1] = -dx;
    	}
    	return n;
    }

    /**
     * create the semi cross section of the pond wall
     * @return
     */
    private double[][] makePond() {
    	double[] r = {0, radius, radius, radius + wallWidth, radius + wallWidth};
    	double[] y = {wallBaseHeight, wallBaseHeight , wallHeight, wallHeight, 0};
    	double[][] pair = {r, y};
    	return pair;
	}

    /**
     * create the semi cross section of the water
     * , the semi cross changes according to the time,
     * , the sine wave shifts to create the animation effect
     * @return
     */
	private double[][] makeRipple() {
        double[] r = new double[pointsPerCycle*5 + 1];     
        double[] y = new double[pointsPerCycle*5 + 1];  
    	for (int j = 0; j <= pointsPerCycle*5; j++) {
            double a = Math.PI * j * 2/ (pointsPerCycle);
            //System.out.println(a);
            r[j] = a * alpha;
            y[j] = waterLevel + ((r.length - j*1.0)/r.length)*rippleAmp*Math.sin(a - (timeCount *Math.PI)/pointsPerCycle);
        }
    	double[][] pair = {r, y};
        timeCount++;
		return pair;
	}

	
	/**
	 * draws the 3d object by Revolving the cross section 
	 * @param r
	 * @param y
	 * @param gl
	 */
	private void drawRevo(double[] r, double[] y, GL2 gl) {
        gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
        int ang,i;  
        int delang = 5;
        double x1,x2,z1,z2,x3,z3;
        double[] n = makeProfileNormals(r,y);
        for (i=0;i<r.length-1;i++) 
        { 
         gl.glBegin(GL2.GL_TRIANGLE_STRIP); 
          for(ang=0;ang<=360;ang+=delang)  
          {       	  
           x1=r[i]*Math.cos((double)ang*2.0*Math.PI/360.0); 
           x2=r[i+1]*Math.cos((double)ang*2.0*Math.PI/360.0); 
           z1=r[i]*Math.sin((double)ang*2.0*Math.PI/360.0);  
           z2=r[i+1]*Math.sin((double)ang*2.0*Math.PI/360.0);  
           
         
           double normal[] = new double[3];
           normal[0] = n[i*2]* Math.cos((double)ang*2.0*Math.PI/360.0); 
           normal[1] = n[i*2+1];
           normal[2] = n[i*2]* Math.sin((double)ang*2.0*Math.PI/360.0); 
           normalize(normal);    
       
           gl.glNormal3dv(normal,0);         
           gl.glVertex3d(x1,y[i],z1);
           normal[0] = n[(i+1)*2]* Math.cos((double)ang*2.0*Math.PI/360.0); 
           normal[1] = n[(i+1)*2+1];
           normal[2] = n[(i+1)*2]* Math.sin((double)ang*2.0*Math.PI/360.0); 
           
           normalize(normal);    
           gl.glNormal3dv(normal,0); 
           gl.glVertex3d(x2,y[i+1],z2); 
         
           }; 
         gl.glEnd();  
        }
		
	}

   
    	
    /**
     * setups pond wall material property
     * @param gl
     */
    private void setupWallMaterial(GL2 gl) {
    	float ambient[] = {1.0f, 1.0f, 1.0f, 1.0f}; 
    	float diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};  
    	float specular[] = {1.0f, 1.0f, 1.0f, 1.0f};   
    	float shininess = 27.8f; 

    	
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, ambient,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, diffuse,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, specular,0); 
    	gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, shininess); 
    }
    
    /**
     * setups water material property
     * @param gl
     */
    private void setupWaterMaterial(GL2 gl) {

    	float ambient[] = {0.0f, 0.0f, 0.5f, 0.5f}; 
    	float diffuse[] = {0.0f, 0.0f, 0.5f, 0.5f}; 
    	float specular[] = {0.5f, 0.5f, 0.5f, 0.5f}; 
    	float shininess = 7.8f; 
    	
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, ambient,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, diffuse,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, specular,0); 
    	gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, shininess); 
    }
    
    /**
     * setups pond floor material property
     * @param gl
     */
    private void setupFloorMaterial(GL2 gl) {
    	float ambient[] = {1.0f, 0.3f, 0.3f, 1.0f}; 
    	float diffuse[] = {1.0f, 0.3f, 0.3f, 1.0f}; 
    	float specular[] = {1.0f, 1.0f, 1.0f, 1.0f}; 
    	float shininess = 27.8f; 
    	
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, ambient,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, diffuse,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, specular,0); 
    	gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, shininess); 
    }
    

	public void init(GL2 gl) {
        myTexture = new MyTexture(gl, "ass2/spec/bricks.png", "png", true);
    }
    
}
