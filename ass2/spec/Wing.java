package ass2.spec;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;


/**
 * uses Segmented extrusions to draw the wing 3d shape
 * @author JCBSH
 *
 */

public class Wing {

    // how much the cross-section is scaled before extrusion

	private static final int MAX_STEP = 2;

	private static final int CHANGE_PER_STEP = 5;
    
    private ArrayList<Point> mySpines;

    // the current extruded mesh
    private List<Polygon> myMesh = null;

    public double wingLength = 0.5;
    private double wingThicken = 0.1;
    private int slicesPerPeriod =  64;
    private double thickenFactor = slicesPerPeriod/wingThicken;
    
    private double radius = 1.0;

	private int count = 0;

	private int change = 1;

	private double amp = 0.15;

	
	private int changeInPeak;
	
	private int peak;
	private int bottom;
	private int maxPeak;
	private boolean maxPeakReached;
	private boolean maxPeakCheckFlag;
	private int numberOfPeak;
	private int numberOfX;

    public Wing() {
        initValues();
        initSpines();
    }
    
    /*initialize the values for generating  a dynamic cross section 
     * 
     */
    private void initValues() {
    	count = 0;
    	change = 1;
    	changeInPeak = CHANGE_PER_STEP;
    	peak = 10;
    	bottom = peak - CHANGE_PER_STEP;
    	maxPeak = changeInPeak * (MAX_STEP) + peak;
    	maxPeakReached = false;
    	maxPeakCheckFlag = false;
    	numberOfPeak = ((maxPeak - peak) /changeInPeak) * 2 - 1;
    	numberOfX = (numberOfPeak + 1) * (peak + changeInPeak) + (numberOfPeak - 1) * changeInPeak + 1;
    	System.out.println(maxPeak);
    	System.out.println(numberOfPeak);
		
	}

    
    public void draw(GL2 gl) {
    	//gl.glPopAttrib();

    	//sets up the material property
    	float ambient[] = {1.0f, 0.8f, 0.2f, 1.0f};  
    	float diffuse[] = {1.0f, 0.8f, 0.2f, 1.0f}; 
    	float specular[] = {1.0f, 0.8f, 0.2f, 1.0f}; 
    	float shininess = 7.8f; 
    	
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, ambient,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, diffuse,0); 
    	gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, specular,0); 
    	gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, shininess); 
    	
        // draw the mesh as a sequence of polygons
        List<Polygon> mesh = getMesh();
        
        if (mesh != null) {
            
            gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
            
            for (Polygon p : mesh) {
                p.draw(gl);                
            }
        }
    }


    /**
     * generates a sin cross section that changes in y value as x value changes
     * @return
     */
    public Polygon sineCrossSection() {
    	Polygon triangle = new Polygon();
    	double factor = (count * 1.0)/maxPeak;
        for (int i = 0; i <= 64; i++) {
            double a = Math.PI * i / (32 * 1.0);
            triangle.addPoint((i / thickenFactor) - (32/thickenFactor), (Math.cos(a) - 1) * amp*factor, 0);
        }
        if (count == peak) {
        	change = -1;
        	if (maxPeakReached == false) {
        		bottom += changeInPeak;
        	} else {
        		//System.out.println("swtcih");
        		maxPeakReached = false;
        	}
        	
        } else if (count == bottom) {
        	change = 1;
        	peak += changeInPeak;
        }
        if (maxPeakCheckFlag  == false) {
            if (peak == maxPeak) {
            	//maxPeak += 10;
            	maxPeakReached = true;
            	maxPeakCheckFlag = true;
            	changeInPeak = -changeInPeak;
            }
        }

        
        count+= change;
		return triangle;
    }

    /**
     * Creates the spine for extrusions
     *
     */
    public void initSpines() {
        // a simple line segment
        mySpines = new ArrayList<Point>();
        for (int i = 0; i < numberOfX; i++) {
        	double x = i*1.0/(numberOfX/wingLength);
        	mySpines.add(new Point(x, 0, 0));
        }
    }



    /**
     * Get the currently selected cross section
     * 
     * @return
     */
    public Polygon getCrossSection() {

        return sineCrossSection();//myCrossSections.get(s);
    }

    /**
     * Get the currently selected spine
     * 
     * @return
     */
    public List<Point> getSpine() {
        return mySpines;
    }
    
    /**
     * Get the extruded mesh
     * 
     * @return
     */
    public List<Polygon> getMesh() {
        // compute the mesh if necessary
        if (myMesh == null) {
            computeMesh();
        }
        return myMesh;
    }
    
    /**
     * The extrusion code.
     * 
     * This method extrudes the cross section along the spine
     *
     */
    private void computeMesh() {
    	initValues();
               
        Polygon cs = getCrossSection();
        
        List<Point> crossSection = cs.getPoints();
        
        
        List<Point> spine = getSpine();
        if (spine == null) {
            return;
        }

        //
        // Step 1: create a vertex list by moving the cross section along the spine
        //
        
        List<Point> vertices = new ArrayList<Point>();

        Point pPrev;
        Point pCurr = spine.get(0);
        Point pNext = spine.get(1);
        
        // first point is a special case
        addPoints(crossSection, vertices, pCurr, pCurr, pNext);
        
        // mid points
        for (int i = 1; i < spine.size() - 1; i++) {
            cs = getCrossSection();          
            crossSection = cs.getPoints();
            pPrev = pCurr;
            pCurr = pNext;
            pNext = spine.get(i+1);
            addPoints(crossSection, vertices, pPrev, pCurr, pNext);            
        }
        
        // last point is a special case
        cs = getCrossSection();
        crossSection = cs.getPoints();
        pPrev = pCurr;
        pCurr = pNext;
        addPoints(crossSection, vertices, pPrev, pCurr, pCurr);

        // 
        // Step 2: connect points in successive cross-sections to form quads
        // 
        
        myMesh = new ArrayList<Polygon>();

        int n = crossSection.size();
        
        // for each point along the spine
        for (int i = 0; i < spine.size() - 1; i++) {

            // for each point in the cross section
            for (int j = 0; j < n; j++) {
                // create a quad joining this point and the next one
                // to the equivalent points in the next cross-section
                
                // note: make sure they are in anti-clockwise order
                // so they are facing outwards
                
                Polygon quad = new Polygon();                
                quad.addPoint(vertices.get(i * n + j));
                quad.addPoint(vertices.get(i * n + (j+1) % n));
                quad.addPoint(vertices.get((i+1) * n + (j+1) % n));
                quad.addPoint(vertices.get((i+1) * n + j));
                myMesh.add(quad);
            }
            
        }
        
    }



	/**
     * Transform the points in the cross-section using the Frenet frame
     * and add them to the vertex list.
     * 
     * @param crossSection The cross section
     * @param vertices The vertex list
     * @param pPrev The previous point on the spine
     * @param pCurr The current point on the spine
     * @param pNext The next point on the spine
     */
    private void addPoints(List<Point> crossSection, List<Point> vertices,
            Point pPrev, Point pCurr, Point pNext) {

        // compute the Frenet frame as an affine matrix
        double[][] m = new double[4][4];
        
        // phi = pCurr        
        m[0][3] = pCurr.x;
        m[1][3] = pCurr.y;
        m[2][3] = pCurr.z;
        m[3][3] = 1;
        
        // k = pNext - pPrev (approximates the tangent)
       
        m[0][2] = pNext.x - pPrev.x;
        m[1][2] = pNext.y - pPrev.y;
        m[2][2] = pNext.z - pPrev.z;
        m[3][2] = 0;
      
        
        // normalise k
        double d = Math.sqrt(m[0][2] * m[0][2] + m[1][2] * m[1][2] + m[2][2] * m[2][2]);  
        m[0][2] /= d;
        m[1][2] /= d;
        m[2][2] /= d;
        
        // i = simple perpendicular to k
        m[0][0] = -m[1][2];
        m[1][0] =  m[0][2];
        m[2][0] =  0;
        m[3][0] =  0;
        
        // j = k x i
        m[0][1] = m[1][2] * m[2][0] - m[2][2] * m[1][0];
        m[1][1] = m[2][2] * m[0][0] - m[0][2] * m[2][0];
        m[2][1] = m[0][2] * m[1][0] - m[1][2] * m[0][0];
        m[3][1] =  0;
        
        // transform the points
       
        for (Point cp : crossSection) {
        
            Point q = cp.transform(m);
           
            vertices.add(q);
        }
    }


}
