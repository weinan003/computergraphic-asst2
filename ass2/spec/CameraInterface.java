package ass2.spec;

import javax.media.opengl.GL2;

public interface CameraInterface {
	public float[] getExactLoc();
	public void Draw();
	public void reshape(GL2 gl, int width, int height);
}
