
package ass2.spec;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.xml.crypto.Data;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * COMMENT: Comment HeightMap
 *
 * @author malcolmr
 */
public class Terrain {

    private static final int CAMERA_FRIST_MODE = 1;
    private static final int CAMERA_THIRD_MODE = 3;
    private static final int CAMERA_GLOBAL_MODE = 0;
	
    private Dimension mySize;
    public static double[][] myAltitude;
    private List<Tree> myTrees;
    private List<Road> myRoads;
    private List<Pond> myPonds;
    private float[] mySunlight;
    private MyTexture myTexture;
    public Camera myCamera;
	private Avatar myAvatar;
	private int myCameraMode;
	private Sun mySun;
	private boolean myNightMode;
	private Rain myRain;
	private boolean myRainMode;
	private VBOItem myVBOItem;

    /**
     * Create a new terrain
     *
     * @param width The number of vertices in the x-direction
     * @param depth The number of vertices in the z-direction
     */
    public Terrain(int width, int depth) {
        mySize = new Dimension(width, depth);
        myAltitude = new double[width][depth];
        myTrees = new ArrayList<Tree>();
        myRoads = new ArrayList<Road>();
        myPonds = new ArrayList<Pond>();
        mySunlight = new float[3];

        myCamera = new Camera(width, depth);
		myCameraMode = CAMERA_GLOBAL_MODE;
		mySun = new Sun();
		myNightMode =  false;
		myRainMode = false;
		myRain = new Rain(width, depth);
		myVBOItem = new VBOItem();
    }

    public Terrain(Dimension size) {
        this(size.width, size.height);
    }

    public Dimension size() {
        return mySize;
    }

    public List<Tree> trees() {
        return myTrees;
    }

    public List<Road> roads() {
        return myRoads;
    }

    public float[] getSunlight() {
        return mySun.getSunPosition();
    	//return mySunlight;
    }

    /**
     * Set the sunlight direction. Note: the sun should be treated as a
     * directional light, without a position
     * 
     * @param dx
     * @param dy
     * @param dz
     */
    public void setSunlightDir(float dx, float dy, float dz) {
        mySunlight[0] = dx;
        mySunlight[1] = dy;
        mySunlight[2] = dz;
        mySunlight = mySun.getSunPosition();
    }

    /**
     * Resize the terrain, copying any old altitudes.
     * 
     * @param width
     * @param height
     */
    public void setSize(int width, int height) {
        mySize = new Dimension(width, height);
        double[][] oldAlt = myAltitude;
        myAltitude = new double[width][height];

        for (int i = 0; i < width && i < oldAlt.length; i++) {
            for (int j = 0; j < height && j < oldAlt[i].length; j++) {
                myAltitude[i][j] = oldAlt[i][j];
            }
        }
    }

    /**
     * Get the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public double getGridAltitude(int x, int z) {
        return myAltitude[x][z];
    }

    /**
     * Set the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public void setGridAltitude(int x, int z, double h) {
        myAltitude[x][z] = h;
    }

    /**
     * Get the altitude at an arbitrary point. Non-integer points should be
     * interpolated from neighbouring grid points TO BE COMPLETED
     * 
     * @param x
     * @param z
     * @return
     */
    public static double altitude(double x, double z) {
        double altitude = 0;
        int x0 = (int) x;
        int x1 = (int) (x + 1);
        int z0 = (int) z;
        int z1 = (int) (z + 1);
        if(x < 0 || z < 0 || x > myAltitude.length - 1 || z > myAltitude[0].length - 1)
            return altitude;
        if(Math.abs(x - x0) < 0.001 && Math.abs(z - z0) < 0.001)
            return myAltitude[x0][z0];
        double k = -1.f;
        double C = z0 - k * x1;
        double judgez = k * x + C;
        x1 = Math.min(x1, myAltitude.length - 1);
        z1 = Math.min(z1, myAltitude.length - 1);
        if (judgez >= z)
        {
            // point in left triangle
            double alt0 = myAltitude[x0][z0];
            double alt1 = myAltitude[x0][z1];
            double alt2 = myAltitude[x1][z0];

            double R1 = ((z - z0) / (z1 - z0)) * alt1 + ((z1 - z) / (z1 - z0)) * alt0;
            double R2 = ((z0 - z) / (z0 - z1)) * alt1 + ((z - z1) / (z0 - z1)) * alt2;
            double X1 = x0;
            double X2 = (z - C) / k;
            altitude = ((X2 - x) / (X2 - X1)) * R1 + ((x - X1) / (X2 - X1)) * R2;
        }
        else
        {
            // point in right triangle
            double alt0 = myAltitude[x0][z1];
            double alt1 = myAltitude[x1][z1];
            double alt2 = myAltitude[x1][z0];
            double R1 = ((z0 - z) / (z0 - z1)) * alt0 + ((z - z1) / (z0 - z1)) * alt2;
            double R2 = ((z0 - z) / (z0 - z1)) * alt1 + ((z - z1) / (z0 - z1)) * alt2;
            double X1 = x1;
            double X2 = (z - C) / k;
            altitude = ((X2 - x) / (X2 - X1)) * R2 + ((x - X1) / (X2 - X1)) * R1;
        }
        //System.out.printf("altidude %f\n",altitude);
        if (altitude != altitude) {
        	//System.out.printf("return 0 altidude");
        	return 0.0;
        	
        } else {
        	//System.out.printf("return altidude %f\n",altitude);
        	return altitude;
        }
    }

    /**
     * Add a tree at the specified (x,z) point. The tree's y coordinate is
     * calculated from the altitude of the terrain at that point.
     * 
     * @param x
     * @param z
     */
    public void addTree(double x, double z) {
        double y = altitude(x, z);
        Tree tree = new Tree(x, y, z);
        //System.out.printf("tree y %f x %f z %f:\n", y, x, z);
        myTrees.add(tree);
    }
    
    
	public void addPond(double x, double z) {
        double y = altitude(x, z);
        Pond pond = new Pond(x, y, z);
        //System.out.printf("tree y %f x %f z %f:\n", y, x, z);
        myPonds.add(pond);
		
	}
	
    
    /**
     * Add an Avatar at the specified (x,z) point. The Avatar's y coordinate is
     * calculated from the altitude of the terrain at that point.
     * 
     * @param x
     * @param z
     */
    public void addAvatar(double x, double z) {
        double y = altitude(x, z);
        //System.out.printf("avatar y %f x %f z %f:\n", y, x, z);
        myAvatar = new Avatar(x, y, z);
    }

    /**
     * Add a road.
     * 
     * @param x
     * @param z
     */
    public void addRoad(double width, double[] spine) {
        Road road = new Road(width, spine);
        myRoads.add(road);
    }

    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();

        //setup global camera or Avatar tracking camera
        switch (myCameraMode) {
        	case(CAMERA_GLOBAL_MODE):
        		myCamera.Draw();
        		break;
        	default:
        		myAvatar.getAvatarCamera().Draw();
        		break;
        }
        // set Light0
        mySunlight = mySun.getSunPosition();
        
        //if night time then disable sunlight and enable flashlight, else the other way around 
        if (myNightMode == false && mySun.isNightTimeStatus() == false) {
        	gl.glDisable(GL2.GL_LIGHT1);
        	
        	gl.glEnable(GL2.GL_LIGHT0);
        	float lightAmb[] = {
                    0.3f, 0.3f, 0.3f, 1.0f
            };
            float lightDifAndSpec0[] = mySun.getSuncolor();
            float lightPos0[] = {
                    10, 10, 10, 1
            };
            
            
            lightPos0[0] *= mySunlight[0];
            lightPos0[1] *= mySunlight[1];
            lightPos0[2] *= mySunlight[2];
            gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, lightAmb, 0);
            gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, lightDifAndSpec0, 0);
            gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, lightDifAndSpec0, 0);
            gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos0, 0);
            float globAmb[] = {0.2f, 0.2f, 0.2f, 1.0f};
            
            gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, globAmb,0); // Global ambient light.
            
            
        } else {
        	gl.glDisable(GL2.GL_LIGHT0);
        	// Turn on OpenGL lighting.
        	gl.glEnable(GL2.GL_LIGHT1);

        	// Light property vectors.
        	float lightAmb[] = {0.0f, 0.0f, 0.0f, 1.0f};
        	float lightDifAndSpec[] = {2.0f, 2.0f, 2.0f, 1.0f};
        	float globAmb[] = {0.1f, 0.1f, 0.1f, 1.0f};

        	// Light properties.
        	gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightAmb,0);
        	gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightDifAndSpec,0);
        	gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, lightDifAndSpec,0);

        	gl.glEnable(GL2.GL_LIGHT1); // Enable particular light source.
        	gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, globAmb,0); // Global ambient light.
        	//gl.glLightModeli(GL2.GL_LIGHT_MODEL_LOCAL_VIEWER, GL2.GL_TRUE); // Enable local viewpoint.
        	myAvatar.turnOnTorch(gl);
        }
        
        

        // Draw Terrian

        float matAmbAndDifL[] = {
                1.f, 1.0f, 1.f, 1.0f
        };
        float matSpecL[] = {
                1.f, 1.f, 1.f, 1.f
        };
        float matShineL[] = {
                50.0f
        };
        DEBUG_COORD(gl);

        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE,
                matAmbAndDifL, 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecL, 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SHININESS, matShineL, 0);

        //divide every rectangle to two trangle
        //every map is organized by n * n rectangle
        //draw left trangle in each rectangle in first loop
        //draw right trangle in each rectangle in second loop
        gl.glBindTexture(GL.GL_TEXTURE_2D, myTexture.getTextureId());
        gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);
        gl.glBegin(GL2.GL_TRIANGLES);
        {
            for (int x = 0; x < myAltitude.length - 1; x++) {
                for (int z = 0; z < myAltitude[0].length - 1; z++) {
                    double[] v1 = {
                            0, myAltitude[x][z + 1] - myAltitude[x][z], 1
                    };
                    double[] v2 = {
                            1, myAltitude[x + 1][z] - myAltitude[x][z + 1], -1
                    };
                    double[] n = MathUtil.normal(MathUtil.crossMultiply(v1, v2));
                    gl.glNormal3d(n[0], n[1], n[2]);
                    gl.glTexCoord2d(0,0);
                    gl.glVertex3d(x, myAltitude[x][z], z);
                    gl.glTexCoord2d(1,0);
                    gl.glVertex3d(x, myAltitude[x][z + 1], z + 1);
                    gl.glTexCoord2d(0,1);
                    gl.glVertex3d(x + 1, myAltitude[x + 1][z], z);
                }
            }

            for (int x = 0; x < myAltitude.length - 1; x++) {
                for (int z = 1; z < myAltitude[0].length; z++) {
                    double[] v1 = {
                            1, myAltitude[x + 1][z] - myAltitude[x][z], 0
                    };
                    double[] v2 = {
                            0, myAltitude[x + 1][z - 1] - myAltitude[x + 1][z], -1
                    };
                    double[] n = MathUtil.normal(MathUtil.crossMultiply(v1, v2));
                    gl.glNormal3d(n[0], n[1], n[2]);
                    gl.glTexCoord2d(1, 0);
                    gl.glVertex3d(x, myAltitude[x][z], z);
                    gl.glTexCoord2d(1, 1);
                    gl.glVertex3d(x + 1, myAltitude[x + 1][z], z);
                    gl.glTexCoord2d(0, 1);
                    gl.glVertex3d(x + 1, myAltitude[x + 1][z - 1], z - 1);
                }
            }
        }
        gl.glEnd();
        // enable polygon offset for filled polygons
        gl.glEnable(GL2.GL_POLYGON_OFFSET_POINT);
        gl.glEnable(GL2.GL_POLYGON_OFFSET_LINE);
        gl.glEnable(GL2.GL_POLYGON_OFFSET_FILL);
        // push this polygon to the front a little
        gl.glPolygonOffset(-8, -8);
        // push to the back a little
        gl.glPolygonOffset(8, 8);
        float[] blackc = {
                0, 0, 0, 1
        };
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, blackc, 0);
        gl.glLineWidth(1);
        gl.glBegin(GL2.GL_LINES);
        {
            for (int x = 0; x < myAltitude.length - 1; x++) {
                for (int z = 0; z < myAltitude[0].length - 1; z++) {
                    gl.glVertex3d(x, myAltitude[x][z], z);
                    gl.glVertex3d(x, myAltitude[x][z + 1], z + 1);

                    gl.glVertex3d(x, myAltitude[x][z + 1], z + 1);
                    gl.glVertex3d(x + 1, myAltitude[x + 1][z], z);

                    gl.glVertex3d(x + 1, myAltitude[x + 1][z], z);
                    gl.glVertex3d(x, myAltitude[x][z], z);
                }
            }

            for (int x = 0; x < myAltitude.length - 1; x++) {
                for (int z = 1; z < myAltitude[0].length; z++) {
                    gl.glVertex3d(x, myAltitude[x][z], z);
                    gl.glVertex3d(x + 1, myAltitude[x + 1][z], z);

                    gl.glVertex3d(x + 1, myAltitude[x + 1][z], z);
                    gl.glVertex3d(x + 1, myAltitude[x + 1][z - 1], z - 1);

                    gl.glVertex3d(x + 1, myAltitude[x + 1][z - 1], z - 1);
                    gl.glVertex3d(x, myAltitude[x][z], z);
                }
            }

        }
        gl.glEnd();

        //draw tree
        for (Tree t : myTrees) {
            t.Draw(gl);
        }

        //draw road
        for (Road r : myRoads) {
            r.Draw(gl);
        }
        
        //draw pond
        for (Pond p : myPonds) {
            p.Draw(gl);
        }


        // if first person view, then dont draw avatar, else draw avatar
        if (myCameraMode == CAMERA_GLOBAL_MODE || myCameraMode == CAMERA_THIRD_MODE) {
        	
            if (myCameraMode == CAMERA_GLOBAL_MODE) {
            	myAvatar.draw(gl, myCamera.getExactLoc());
            } else {
            	myAvatar.draw(gl, myAvatar.getAvatarCamera().getExactLoc());
            }
            
        }
        
        //draw rain, make rain drops face global camera if it is enable, else make it face the avator camter
        if (myRainMode == true) {
            if (myCameraMode == CAMERA_GLOBAL_MODE) {
            	myRain.display(gl, myCamera.getExactLoc());
            } else {
            	myRain.display(gl, myAvatar.getAvatarCamera().getExactLoc());
            }
        }


       myVBOItem.display(drawable);
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL2 gl = drawable.getGL().getGL2();
        myCamera.reshape(gl, width, height);
    }

    public void dispose(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub

    }

    public void init(GLAutoDrawable drawable) {
    	//System.out.println("init");
        GL2 gl = drawable.getGL().getGL2();
        
        myTexture = new MyTexture(gl, "ass2/spec/grass.bmp", "bmp", true);
        for(Road r : myRoads)
            r.init(gl);
        
        for(Tree t: myTrees)
            t.init(gl);
        
        for(Pond p: myPonds)
            p.init(gl);
        
        addAvatar(1, 1);
         
        myAvatar.init(gl);
        myRain.init(gl);
        myVBOItem.init(gl);
    }

    public void DEBUG_COORD(GL2 gl) {
        gl.glLineWidth(2);
        gl.glBegin(GL2.GL_LINES);
        {
            float[] xco = {
                    1.f, 0.f, 0.f, 1.f
            };
            gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, xco, 0);
            gl.glVertex3d(0, 0, 0);
            gl.glVertex3d(3, 0, 0);

            float[] yco = {
                    1.f, 1.f, 0.f, 1.f
            };
            gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, yco, 0);
            gl.glVertex3d(0, 0, 0);
            gl.glVertex3d(0, 3, 0);

            float[] zco = {
                    0.f, 0.f, 1.f, 1.f
            };
            gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, zco, 0);
            gl.glVertex3d(0, 0, 0);
            gl.glVertex3d(0, 0, 3);
        }
        gl.glEnd();
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
            	if (myCameraMode ==  CAMERA_GLOBAL_MODE) {
            		myCamera.alpha += 0.1;
            	} else {
            		myAvatar.changeAlpha(0.1);
            	}
                break;
            case KeyEvent.VK_LEFT:
            	if (myCameraMode ==  CAMERA_GLOBAL_MODE) {
            		myCamera.alpha -= 0.1;
            	} else {
            		myAvatar.changeAlpha(-0.1);
            	}
                break;
            case KeyEvent.VK_UP:
            	if (myCameraMode ==  CAMERA_GLOBAL_MODE) {
            		myCamera.myRadius -= 0.1;
            	} else {
            		//System.out.println("move front");
            		myAvatar.moveFoward();
            	}
            	
                
                break;
            case KeyEvent.VK_DOWN:
            	if (myCameraMode ==  CAMERA_GLOBAL_MODE) {
            		myCamera.myRadius += 0.1;
            	} else {
            		//System.out.println("move back");
            		myAvatar.moveBackward();
            	}
                break;
            case KeyEvent.VK_U:
            	if (myCameraMode ==  CAMERA_GLOBAL_MODE) {
            		myCamera.beta += 0.1;
                    myCamera.beta = Math.min(Math.PI / 2, myCamera.beta);
                    myCamera.beta = Math.max(-Math.PI / 2, myCamera.beta);
            	} else {
            		myAvatar.changeBeta(0.1);
            	}
                
                break;
            case KeyEvent.VK_D:
            	if (myCameraMode ==  CAMERA_GLOBAL_MODE) {
            		myCamera.beta -= 0.1;
                    myCamera.beta = Math.min(Math.PI / 2, myCamera.beta);
                    myCamera.beta = Math.max(-Math.PI / 2, myCamera.beta);
            	} else {
            		myAvatar.changeBeta(-0.1);
            	}
                
                break;
            case KeyEvent.VK_C:
                myCameraMode = CAMERA_FRIST_MODE;
                myAvatar.turnOnFirstMode();
                break;
            case KeyEvent.VK_V:
            	myCameraMode = CAMERA_THIRD_MODE;
            	myAvatar.turnOnThirdMode();
                break;
            case KeyEvent.VK_B:
            	myCameraMode = CAMERA_GLOBAL_MODE;
                break;
                
            case KeyEvent.VK_M:
            	if (myNightMode == false) mySun.increaseHour();
                break;
            case KeyEvent.VK_N:
            	if (myNightMode == false) mySun.decreaseHour();
                break;
                
            case KeyEvent.VK_L:
            	if (myNightMode) {
            		myNightMode = false;
            	} else {
            		if (mySun.isNightTimeStatus() != true) myNightMode = true;
            		mySun.setTimeLapseMode(false);
            	}
            	
                break;
            case KeyEvent.VK_R:
            	if (myRainMode) {
            		myRainMode = false;
            	} else {
            		myRainMode = true;
            	}
            	
                break;
		  case KeyEvent.VK_K:           	
			if (myNightMode == false) {
				mySun.changeTimeLapseMode();
			}
		    break;
        }
    }
    
    
    public static double globalAltitude(double x, double z) {
    	int x0 = (int) x;
        int x1 = (int) (x + 1);
        int z0 = (int) z;
        int z1 = (int) (z + 1);
        
    	if (x0 >= 0 && x0 < myAltitude.length) {
    		if (z0 >= 0 && z0 < myAltitude[x0].length) {
    			if (x1 >= 0 && x1 < myAltitude.length) {
    	    		if (z1 >= 0 && z1 < myAltitude[x1].length) {
    	        		return altitude(x, z);
    	        	}
    	    	}
        	}
    	}
		return 0;
    } 
    
}
