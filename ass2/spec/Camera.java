package ass2.spec;

import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

public class Camera implements CameraInterface{
    public double myRadius;
    private double mySpotx;
    private double mySpotz;
    public double alpha;
    public double beta;
    public int fov;
    private float exactLoc[] = new float[3];
    public Camera(int w,int d) {
       myRadius = w>d?w:d;
       myRadius += 4;
       mySpotx = w/2.0;
       mySpotz = d/2.0;
       alpha = 0.0;
       beta = 0.0;
       fov = 45;
    }
    
    @Override
    public void reshape(GL2 gl, int width, int height) {
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        double ratio = 1.0 * width / height;
        GLU glu = new GLU();
        glu.gluPerspective(fov, ratio, 1, 200);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }
    
    @Override
    public void Draw() {
        double x = mySpotx + Math.cos(alpha) * myRadius;
        double z = mySpotz + Math.sin(alpha) * myRadius;
        double y = Math.sin(beta)* myRadius;
        
        GLU glu = new GLU();
        glu.gluLookAt(x, y , z, mySpotx, 0, mySpotz, 0, 1, 0);
        exactLoc[0] = (float) x;
    	exactLoc[1] = (float) y;
    	exactLoc[2] = (float) z;
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
                alpha += 0.1;
                break;
            case KeyEvent.VK_LEFT:
                alpha -= 0.1;
                break;
            case KeyEvent.VK_UP:
                myRadius -= 0.1;
                break;
            case KeyEvent.VK_DOWN:
                myRadius += 0.1;
                break;
            case KeyEvent.VK_U:
                beta += 0.1;
                beta = Math.min(Math.PI / 2, beta);
                beta = Math.max(-Math.PI / 2, beta);
                break;
            case KeyEvent.VK_D:
                beta -= 0.1;
                beta = Math.min(Math.PI / 2, beta);
                beta = Math.max(-Math.PI / 2, beta);
                break;
        }
        
    }

	@Override
	public float[] getExactLoc() {
		return exactLoc;
	}

}
