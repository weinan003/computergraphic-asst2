
package ass2.spec;

import java.awt.event.KeyEvent;

import javax.media.opengl.glu.GLU;

public class WanderCamera extends Camera {
    private static final double DISTANCE = 1.5;
    private double m_posX;
    private double m_posY;
    private double m_posZ;
    private double m_Angle;

    public WanderCamera(int w, int d) {
        super(w, d);
        m_posX = w / 2.0;
        m_posZ = d / 2.0;
        m_posY = Terrain.altitude(m_posX, m_posZ) + DISTANCE;
        fov = 45;
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
                m_Angle += 0.1;
                break;
            case KeyEvent.VK_LEFT:
                m_Angle -= 0.1;
                break;
            case KeyEvent.VK_UP:
                m_posX += Math.cos(m_Angle);
                m_posZ += Math.sin(m_Angle);
                m_posY = Terrain.altitude(m_posX, m_posZ) + DISTANCE;
                break;
            case KeyEvent.VK_DOWN:
                m_posX -= Math.cos(m_Angle);
                m_posZ -= Math.sin(m_Angle);
                m_posY = Terrain.altitude(m_posX, m_posZ) + DISTANCE;
                break;
        }
    }
    
    public void Draw() {
        double x = m_posX + Math.cos(m_Angle);
        double z = m_posZ + Math.sin(m_Angle);
        double y = m_posY;
        
        GLU glu = new GLU();
        glu.gluLookAt(m_posX, m_posY , m_posZ, x, y, z, 0, 1, 0);
    }
}
