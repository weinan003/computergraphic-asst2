package ass2.spec;


import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Random;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

import ass2.spec.Rain.Particle;

import com.jogamp.opengl.util.FPSAnimator;

//Implements a surface of revolution
//Takes a 2D profile and sweeps it around the y-axis

public class Avatar{
    private int maxStacks = 20;
    private int maxSlices = 24;
    private int numStacks = 20;
    private int numSlices = 24;
    private int eyeLidCount = 0;
    private int changeLidCount = 1;
    
    private String textureFileName1 = "ass2/spec/eyeTextureNew2.jpg";
   
    private String textureExt1 = "bmp";
    private double[] myPos;
    private MyTexture myTexture;
	private AvatarCamera myAvatarCamera;
	
	private double alpha;
	private double beta;
	    
	private double floatingHeight = 0.3;
	
	double radius = 0.25;
	private float spotAngle = 80.0f;
	private float spotDirection[] = {0.0f, -1.0f, 0.0f}; // Spotlight direction.
	private float spotExponent = 10.0f;
	
	private static final int  MAX_PARTICLE= 10;
	private Particle[] particles =  new Particle[MAX_PARTICLE]; 
	private static int FAN_COUNT = 5;
	private float particleRadius = 0.015f;
	private Wing myWing;
	
	private static final int MAX_WING_FLAP_ANGLE = 30;
	int currentFlapAngle = 0;
	int deltaFlap = 2;
	
    public Avatar(double x, double y, double z) {
        myPos = new double[3];
        myPos[0] = x;
        myPos[1] = y + floatingHeight;
        myPos[2] = z;
        myAvatarCamera = new AvatarCamera(myPos[0], myPos[1], myPos[2]);
        initParticles();
        myWing =  new Wing();
	}



	public void normalize(double v[])  
    {  
        double d = Math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);  
        if (d != 0.0) 
        {  
           v[0]/=d; 
           v[1]/=d;  
           v[2]/=d;  
        }  
    } 
    
    void normCrossProd(double v1[], double v2[], double out[])  
    {  
       out[0] = v1[1]*v2[2] - v1[2]*v2[1];  
       out[1] = v1[2]*v2[0] - v1[0]*v2[2];  
       out[2] = v1[0]*v2[1] - v1[1]*v2[0];  
       normalize(out);  
    } 
    
   
    
    double r(double t){
    	double x  = Math.cos(2 * Math.PI * t);
        return x;
    }
    
    double getY(double t){
    	
    	double y  = Math.sin(2 * Math.PI * t);
        return y;
    }
    

    public void draw(GL2 gl, float[] exactLoc) {
    	
        float[] diffuse = {
                (float) 1, (float) 1, (float) 1, 1
        };
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, diffuse, 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, diffuse, 0);

        gl.glPushMatrix();
        gl.glTranslated(myPos[0], myPos[1], myPos[2]);
        //System.out.printf(" alpha %f\n", MathUtil.normaliseAngle(alpha));
        gl.glRotated(Math.toDegrees(-(alpha - Math.PI/2)),0,1,0);
        gl.glBindTexture(GL2.GL_TEXTURE_2D, myTexture.getTextureId());
    	gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
    	drawObject(gl);
    	

    	//rotating and translates the angle and position of the wing
    	gl.glTranslated(radius, 0, 0);
    	gl.glRotated(-Math.toDegrees(Math.PI/2),0,1,0);
    	gl.glRotated(-Math.toDegrees(Math.PI),0,0,1);
    	gl.glTranslated(-myWing.wingLength/2, 0, 0);
    	
    	gl.glRotated(currentFlapAngle,1,0,0);
    	myWing.draw(gl);
    	gl.glRotated(-currentFlapAngle,1,0,0);
    	
    	gl.glTranslated(myWing.wingLength/2, 0, 0);
    	gl.glRotated(2*Math.toDegrees(Math.PI),0,0,1);
    	gl.glRotated(Math.toDegrees(Math.PI/2),0,1,0);
    	
    	
    	gl.glTranslated(-(2 * radius), 0, 0);
    	gl.glRotated(Math.toDegrees(Math.PI/2),0,1,0);
    	gl.glRotated(-Math.toDegrees(Math.PI),0,0,1);
    	gl.glTranslated(-myWing.wingLength/2, 0, 0);
    	
    	gl.glRotated(-currentFlapAngle,1,0,0);
    	myWing.draw(gl);
    	
    	gl.glPopMatrix();   
    	
    	gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL); 
    	drawParticles(gl, exactLoc);
    	
    	
    	//adjust the angle of the wings to animate the wings 
    	if (currentFlapAngle ==  MAX_WING_FLAP_ANGLE || currentFlapAngle == -MAX_WING_FLAP_ANGLE) {
    		deltaFlap = -deltaFlap;
    	}
    	
    	currentFlapAngle += deltaFlap;
    	
    }
    
    
    /**
     * the star particle are draw facing towards the camera
     * @param gl
     * @param exactLoc
     */
    private void drawParticles(GL2 gl, float[] exactLoc) {

       for (int i = 0; i < particles.length; i++) {

     	 gl.glPushMatrix();
     	  

 		 particles[i].shiftPosition((float) myPos[0], (float) myPos[1], (float)myPos[2]);

     	 
     	 gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, particles[i].colour,0);
     	 
     	 gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, particles[i].colour,0);
     	 
         float px = particles[i].x;
         float py = particles[i].y;
         float pz = particles[i].z;
         
         float[] ray = new float[3];
         ray[0] = px - exactLoc[0];
         ray[1] = py - exactLoc[1];
         ray[2] = pz - exactLoc[2];
         double alpha = 0.0;

         double beta = 0.0 ;

         if (ray[2] != 0.0) {
         	//System.out.println("!!!!!!");
         	alpha = Math.toDegrees(Math.atan(ray[0]/ray[2]));
         	float ZX = (float) (ray[2] / Math.cos(Math.toRadians(alpha)));
         	beta = Math.toDegrees(Math.atan(ray[1]/ZX));
         	//System.out.printf("beta %f\n", beta);
         } else {
         	ray[2] = 0.001f; 
         	//System.out.println("!!!!!!");
         	alpha = Math.toDegrees(Math.atan(ray[0]/ray[2]));
         	float ZX = (float) (ray[2] / Math.cos(Math.toRadians(alpha)));
         	beta = Math.toDegrees(Math.atan(ray[1]/ZX));
         	//System.out.printf("beta %f\n", beta);
         }
          
         
         gl.glTranslated(px, py, pz);
         gl.glRotated((alpha + 360) % 360,0,1,0);
         //gl.glRotated((beta + 360) % 360,1*Math.tan(alpha - 90),0,1);
         gl.glRotated((-beta + 360) % 360, 1, 0, 0);
        
        
         double angle = 360/FAN_COUNT;
         for (int i1 = 0; i1 <= FAN_COUNT; i1++) {
        	gl.glBegin(GL2.GL_POLYGON);

         	
         	double radians = Math.toRadians(i1*angle);
         	
         	float x0 = 0;
         	float y0 = 0;
         	
         	float x1 = (float) (particleRadius*Math.cos(radians));
         	float y1 = (float) (particleRadius*Math.sin(radians));
         	
         	radians = Math.toRadians((i1 + 0.5) *angle);
         	float x2 = (float) (particleRadius*2*Math.cos(radians));
         	float y2 = (float) (particleRadius*2*Math.sin(radians));
         			
         	radians = Math.toRadians((i1 + 1) *angle);
         	float x3 = (float) (particleRadius*Math.cos(radians));
         	float y3 = (float) (particleRadius*Math.sin(radians));
         	
         	gl.glVertex3f(x0,y0,0);
         	gl.glVertex3f(x1,y1,0);
         	gl.glVertex3f(x2,y2,0);
         	gl.glVertex3f(x3,y3,0);
         	
         	gl.glEnd();
         }

        
         
         gl.glPopMatrix();
       }
         
	}

	/**draws the Avatar eye using Revolution method, the eye lid closing is done by keeping track of the angle of the eye lid
	 * 
	 * @param r
	 * @param y
	 * @param gl
	 */
	public void drawObject(GL2 gl){
    	double deltaT;
    
    	
    	deltaT = 0.5/maxStacks;
    	int ang;  
    	int delang = 360/maxSlices;
    	double x1,x2,z1,z2,y1,y2;
    	for (int i = 0; i < numStacks; i++) 
    	{ 
    		double t = -0.25 + i*deltaT;
    		
    		gl.glBegin(GL2.GL_TRIANGLE_STRIP); 
    		for(int j = 0; j <= numSlices; j++)  
    		{  
    			ang = j*delang;
    			x1=radius * r(t)*Math.cos((double)ang*2.0*Math.PI/360.0); 
    			x2=radius * r(t+deltaT)*Math.cos((double)ang*2.0*Math.PI/360.0); 
    			y1 = radius * getY(t);

    			z1=radius * r(t)*Math.sin((double)ang*2.0*Math.PI/360.0);  
    			z2= radius * r(t+deltaT)*Math.sin((double)ang*2.0*Math.PI/360.0);  
    			y2 = radius * getY(t+deltaT);

    			double normal[] = {x1,y1,z1};


    			normalize(normal);    

    			gl.glNormal3dv(normal,0);  
    			double tCoord = 1.0/numStacks * i; //Or * 2 to repeat label
    			double sCoord = 2.0/numSlices * j;
    			if ((j >= eyeLidCount && j <= (numSlices/2 - eyeLidCount)) ||
    					(j >= (eyeLidCount + numSlices/2) && j <= (numSlices - eyeLidCount))) {
    				gl.glTexCoord2d(sCoord,tCoord);
    			} else {
    				gl.glTexCoord2d(0,1);
    			}
    			gl.glVertex3d(x1,y1,z1);
    			normal[0] = x2;
    			normal[1] = y2;
    			normal[2] = z2;

    			
    			normalize(normal);    
    			gl.glNormal3dv(normal,0); 
    			tCoord = 1.0/numStacks * (i+1); //Or * 2 to repeat label
    			if ((j >= eyeLidCount && j <= (numSlices/2 - eyeLidCount)) ||
    					(j >= (eyeLidCount + numSlices/2)  && j <= (numSlices - eyeLidCount))) {
    				gl.glTexCoord2d(sCoord,tCoord);
    			} else {
    				gl.glTexCoord2d(0,1);
    			}
    			//gl.glTexCoord2d(sCoord,tCoord);
    			gl.glVertex3d(x2,y2,z2); 

    		}; 
    		gl.glEnd();  
    	}
    	
    	if (eyeLidCount == 0) {
    		changeLidCount = 1;
    	} else if (eyeLidCount == 6) {
    		changeLidCount = -1;
    	}
    	
    	eyeLidCount += changeLidCount;
    	
    
    }


    public void init(GL2 gl) {

    	myTexture = new MyTexture(gl,textureFileName1,textureExt1,true);

    	  
    } 	   
    	 


	public AvatarCamera getAvatarCamera() {
		// TODO Auto-generated method stub
		return myAvatarCamera;
	}

	/**
	 * changes the sideway angles the avatar is facing
	 * @param d
	 */
	public void changeAlpha(double d) {
		alpha += d;
		myAvatarCamera.alpha = alpha;
		
	}

	
	/**
	 * changes the elevation angle of the avatar is  and its camera
	 * @param d
	 */
	public void changeBeta(double d) {
		beta += d;
        beta = Math.min((Math.PI / 2) - 0.1, beta);
        beta = Math.max((-Math.PI / 2) + 0.1, beta);
        myAvatarCamera.beta = beta;
		
	}
	
	/**
	 * move avatar and its camera
	 */
	public void moveBackward() {
		myPos[0] = myPos[0] +  Math.cos(alpha) * -0.1;
		myPos[2] = myPos[2] +  Math.sin(alpha) * -0.1;
		myPos[1] = Terrain.globalAltitude(myPos[0], myPos[2]) + floatingHeight;
		myAvatarCamera.setPosition(myPos[0],myPos[1], myPos[2]);
		
	}

	public void moveFoward() {
		myPos[0] = myPos[0] +  Math.cos(alpha) * 0.1;
		myPos[2] = myPos[2] +  Math.sin(alpha) * 0.1;
		myPos[1] = Terrain.globalAltitude(myPos[0], myPos[2]) + floatingHeight;
		myAvatarCamera.setPosition(myPos[0],myPos[1], myPos[2]);
		
	}



	public void turnOnFirstMode() {
		myAvatarCamera.turnOnFirstMode();
		
	}

	public void turnOnThirdMode() {
		myAvatarCamera.turnOnThirdMode();
		
	}

	/**
	 * setup the spotlight position and direction according to avators's position and direction
	 * @param gl
	 */
    public void turnOnTorch(GL2 gl) {
        gl.glPushMatrix();{
        	//gl.glTranslated(myPos[0], 0, myPos[2]); // Move the spotlight.
        	float xLight = (float) (myPos[0]);// - Math.cos(alpha));
        	float zLight = (float) (myPos[2]);// - Math.sin(alpha));
        	float lightPos[] = {xLight, (float) (myPos[1] + radius), zLight, 1.0f}; // Spotlight position.
        	gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPos,0);  

        	// Spotlight properties including position.
        	spotDirection[0] = (float) Math.cos(alpha);
        	spotDirection[2] = (float) Math.sin(alpha);
        	spotDirection[1] = (float) -0.25;
            
        	gl.glLightf(GL2.GL_LIGHT1, GL2.GL_SPOT_CUTOFF, spotAngle);
        	gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPOT_DIRECTION, spotDirection,0);    
        	gl.glLightf(GL2.GL_LIGHT1, GL2.GL_SPOT_EXPONENT, spotExponent);

        }gl.glPopMatrix();

    }


	private void initParticles() {
		for (int i = 0; i < MAX_PARTICLE; i++) {
			particles[i] = new Particle((float) myPos[0], (float) myPos[1], (float)myPos[2], i*(360/MAX_PARTICLE), (float) radius);
		}
		
	}
    
    // Particle (inner class)
    class Particle {
       float colour[] = {0.0f, 0.0f, 0.5f, 0.25f};  // color
       float x, y, z;  // position
       int degree;
       int deltaAng = 1;
       float radius;
       
       private final float[][] colors = {    // rainbow of 12 colors
               { 1.0f, 0.5f, 0.5f }, { 1.0f, 0.75f, 0.5f }, { 1.0f, 1.0f, 0.5f },
               { 0.75f, 1.0f, 0.5f }, { 0.5f, 1.0f, 0.5f }, { 0.5f, 1.0f, 0.75f },
               { 0.5f, 1.0f, 1.0f }, { 0.5f, 0.75f, 1.0f }, { 0.5f, 0.5f, 1.0f },
               { 0.75f, 0.5f, 1.0f }, { 1.0f, 0.5f, 1.0f }, { 1.0f, 0.5f, 0.75f } };
     private Random rand = new Random();


       // Constructor
       public Particle(float x, float y,  float z, int degree, float radius) {
    	   //randomizePosition(x, y, z);
           int colorIndex = rand.nextInt((colors.length - 1));
           // Pick a random color
           colour[0] = colors[colorIndex][0];
           colour[0] = colors[colorIndex][1];
           colour[0] = colors[colorIndex][2];
           this.x = (float) (x + radius*2*Math.cos( Math.toRadians(degree)));
    	   this.z = (float) (z + radius*2*Math.sin( Math.toRadians(degree)));
           
           this.y = (float) (y + floatingHeight);;
           this.degree = degree;
           this.radius = radius;

       }
        
       
       /**
        * the Star Particle changes its position relative to the avatar, this makes it appear spinning 
        * 
        * @param x value of Avatar
        * @param y value of Avatar
        * @param z value of Avatar
        */
       public void shiftPosition (float x, float y,  float z) {
    	   degree += deltaAng;
    	   //System.out.println(degree);
    	   this.x = (float) (x + radius*2*Math.cos( Math.toRadians(degree)));
    	   this.z = (float) (z + radius*2*Math.sin( Math.toRadians(degree)));
    	   this.y = (float) (y + floatingHeight);;
       }
       
    }

}
