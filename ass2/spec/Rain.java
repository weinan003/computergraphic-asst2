package ass2.spec;

import java.awt.event.*;
import java.util.Random;

import javax.swing.*;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;

import com.jogamp.opengl.util.FPSAnimator;

import static java.awt.event.KeyEvent.VK_T;
import static javax.media.opengl.GL.*;  // GL constants
import static javax.media.opengl.GL2.*; // GL2 constants


/**
 * 
 */
public class Rain {
   
	private static final int RAIN_DENSITY = 8;
   private Particle[] particles; 

   private int mWidth;
   private int mLength;
   private int mHeight = 10;;
   
   private float radius = 0.015f;
   
   private static int FAN_COUNT = 12;
   

   public Rain (int width, int length) {
	   mWidth = width;
	   mLength = length;
	   particles = new Particle[width*length*RAIN_DENSITY];
   }
   

   public void init(GL2 gl) {  
      generateDrops();
   }

   /**
    * generates density times number of tiles of rain drop particles
    */
   private void generateDrops() {
	   int count = 0;
	   for (int i = 0; i < mWidth; i++) {
		   for (int j = 0; j < mLength; j++) {
			   
			   for (int k = 0; k < RAIN_DENSITY; k++) {	// makes the rain drops distribute evenly to make the rain look more real
				   particles[count] = new Particle(i, j);
				   count++;
			   }
		   }
	   }	   
   }

   /**
    * 
    * @param gl
    * @param exactLoc is the camera location that all particle need to face 
    */
   public void display(GL2 gl, float[] exactLoc) {
      
	   //alpha blends all particles 
	   	gl.glBlendFunc(GL2.GL_SRC_ALPHA,GL2.GL_ONE_MINUS_SRC_ALPHA);
	   	gl.glEnable(GL2.GL_BLEND);
      // Render the particles
      for (int i = 0; i < particles.length; i++) {
    	  gl.glPushMatrix();
    	  {
        	 
        	 //sets the material property of the rain drops
        	 gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, particles[i].colour,0);
        	 
            float px = particles[i].x;
            float py = particles[i].y;
            float pz = particles[i].z;
            
            //works out the two angle the rain drops need to rotate to face camera
            float[] ray = new float[3];
            ray[0] = px - exactLoc[0];
            ray[1] = py - exactLoc[1];
            ray[2] = pz - exactLoc[2];
            double alpha = 0.0;
            double beta = 0.0 ;
            if (ray[2] != 0.0) {
            	alpha = Math.toDegrees(Math.atan(ray[0]/ray[2]));
            	float ZX = (float) (ray[2] / Math.cos(Math.toRadians(alpha)));
            	beta = Math.toDegrees(Math.atan(ray[1]/ZX));
            } else { //a way to handle when the rain drop has the same z value as the camera
            	ray[2] = 0.005f; 
            	alpha = Math.toDegrees(Math.atan(ray[0]/ray[2]));
            	float ZX = (float) (ray[2] / Math.cos(Math.toRadians(alpha)));
            	beta = Math.toDegrees(Math.atan(ray[1]/ZX));
            }
             
            //rotate the y , then x axis to face the camera before drawing the rain drops
            gl.glTranslated(px, py, pz);
            gl.glRotated((alpha + 360) % 360,0,1,0);
            gl.glRotated((-beta + 360) % 360, 1, 0, 0);
            
            
            
            gl.glBegin(GL2.GL_TRIANGLE_FAN);; // build circle from a triangle FAN
            double angle = 360/FAN_COUNT;
            for (int i1 = 0; i1 <= FAN_COUNT; i1++) {
            	
            	double radians = Math.toRadians(i1*angle);
            	gl.glVertex3f((float) (radius*Math.cos(radians)),(float) (radius*Math.sin(radians)), 0);
            }
            	
            
            gl.glEnd();

            particles[i].drop();

         }
         gl.glPopMatrix();
      }
      gl.glDisable(GL2.GL_BLEND);
   }


   // Particle (inner class)
   class Particle {
      float colour[] = {0.0f, 0.0f, 0.5f, 0.25f};  // color
      float x, y, z;  // position
      float orgX, orgZ;  // position
      float speed = 0.03f;
      private int factor = 100;


      private Random rand = new Random();

      // Constructor
      /**
       * y is random to makes the rain feel its not falling at the same time
       * @param x
       * @param z
       */
      public Particle(float x, float z) {
    	  orgX = x;
    	  orgZ = z;
         this.x = (float) (orgX + rand.nextInt(factor)/(factor * 1.0));
         this.z = (float) (orgZ + rand.nextInt(factor)/(factor * 1.0));
         this.y = (float) (mHeight * (rand.nextInt(1000)/1000.0));

      }

      
      /**
       * y value reduce according to its speed, then
       * the y position is reset to the highest point if the rain reaches bottom
       * and the x and z position is randomized within the bound a tile 
       * 
       */
	public void drop() {
		
		y -= speed;
		if (y < 0) {
			y = mHeight;
	         this.x = (float) (orgX + rand.nextInt(factor)/(factor * 1.0));
	         this.z = (float) (orgZ + rand.nextInt(factor)/(factor * 1.0));
		}
		
	}


   }





}
