package ass2.spec;

import java.util.Calendar;
import java.util.Date;

/**
 * a object to model the sun's info, its's position, color and movement
 * @author JCBSH
 *
 */
public class Sun {
    
    double sunAngle = 0.0;
    float radius;
    int hour;
    float[] sunColor = new float[4];
    float[] pos = new float[3];
    private boolean nightTimeStatus =  false;
    private int getCount = 0;
    private int getsPerShift = 5;
    private boolean timeLapseMode = false;
    
    public Sun () {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        updateInfo();
        radius = 2;
    }
    
    
    
    public float[] getSunPosition () {
        if (timeLapseMode) {
            if (getCount == getsPerShift) {
                getCount = 0;
                hour = ((hour + 1) + 24) % 24;
                updateInfo();
            }
            
            getCount++;
        }
        return pos; 
    }
    
    public float[] getSuncolor () {
        return sunColor;    
    }
    
    public void increaseHour() {
        hour = ((hour + 1) + 24) % 24;
        updateInfo();
    }
    
    public void decreaseHour() {
        hour = ((hour - 1) + 24) % 24;
        updateInfo();
    }
    private void updateInfo() {
        //System.out.println(hour);
        updateAngle();
        updateSunPosition();
        updateColor();
        updateNightTImeStatus();
    }
    
    private void updateColor() {
        sunColor[0] = 1.f;
        sunColor[1] = (float) (0.6 + 0.4*Math.sin(sunAngle));  
        sunColor[2] = (float) (0.6 + 0.4*Math.sin(sunAngle));  
        sunColor[3] = 1.0f;
    }
    
    private void updateSunPosition() {
        pos[0] = (float) (radius * Math.cos(sunAngle));
        pos[1] = (float) (radius * Math.sin(sunAngle));
        pos[2] = 5;
    }
    
    /**
     * angle of the sun change as hour changes
     */
    private void updateAngle() {
        int i = ((hour - 6) + 24) % 24;
        double degree = i * (360/24);
        sunAngle = Math.toRadians(degree);
        
    }
    
    /**
     * day time night time info is updated
     */
    private void updateNightTImeStatus() {
        if (hour >= 6 && hour <= 18) {
            nightTimeStatus = false;
        } else {
            nightTimeStatus = true;
        }
    }


    /**
     * 
     * @return whether its day or night
     */
    public boolean isNightTimeStatus() {
        return nightTimeStatus;
    }



    /**
     * the sun position will change according to passing of time
     * or it will pause.
     */
    public void changeTimeLapseMode() {
        timeLapseMode = !timeLapseMode; 
    }



	public void setTimeLapseMode(boolean b) {
		timeLapseMode = b;
		
	}

}
