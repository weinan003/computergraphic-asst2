
package ass2.spec;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

public class Road {

    private List<Double> myPoints;
    private double myWidth;
    private MyTexture myTexture;

    /**
     * Create a new road starting at the specified point
     */
    public Road(double width, double x0, double y0) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        myPoints.add(x0);
        myPoints.add(y0);
    }

    /**
     * Create a new road with the specified spine
     *
     * @param width
     * @param spine
     */
    public Road(double width, double[] spine) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        for (int i = 0; i < spine.length; i++) {
            myPoints.add(spine[i]);
        }
    }

    /**
     * The width of the road.
     * 
     * @return
     */
    public double width() {
        return myWidth;
    }

    /**
     * Add a new segment of road, beginning at the last point added and ending
     * at (x3, y3). (x1, y1) and (x2, y2) are interpolated as bezier control
     * points.
     * 
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     */
    public void addSegment(double x1, double y1, double x2, double y2, double x3, double y3) {
        myPoints.add(x1);
        myPoints.add(y1);
        myPoints.add(x2);
        myPoints.add(y2);
        myPoints.add(x3);
        myPoints.add(y3);
    }

    /**
     * Get the number of segments in the curve
     * 
     * @return
     */
    public int size() {
        return myPoints.size() / 6;
    }

    /**
     * Get the specified control point.
     * 
     * @param i
     * @return
     */
    public double[] controlPoint(int i) {
        double[] p = new double[2];
        p[0] = myPoints.get(i * 2);
        p[1] = myPoints.get(i * 2 + 1);
        return p;
    }

    /**
     * Get a point on the spine. The parameter t may vary from 0 to size().
     * Points on the kth segment take have parameters in the range (k, k+1).
     * 
     * @param t
     * @return
     */
    public double[] point(double t) {
        int i = (int) Math.floor(t);
        t = t - i;

        i *= 6;

        double x0 = myPoints.get(i++);
        double y0 = myPoints.get(i++);
        double x1 = myPoints.get(i++);
        double y1 = myPoints.get(i++);
        double x2 = myPoints.get(i++);
        double y2 = myPoints.get(i++);
        double x3 = myPoints.get(i++);
        double y3 = myPoints.get(i++);

        double[] p = new double[2];

        p[0] = b(0, t) * x0 + b(1, t) * x1 + b(2, t) * x2 + b(3, t) * x3;
        p[1] = b(0, t) * y0 + b(1, t) * y1 + b(2, t) * y2 + b(3, t) * y3;

        return p;
    }

    public void Draw(GL2 gl) {
        int size = size();
        double step = 0.01;
        gl.glPushMatrix();
        float[] diffuse = {
                (float) 1, (float) 1, (float) 1, 1
        };
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, diffuse, 0);
        gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
        gl.glLineWidth(2);
        int TIMES = 100;
        for (int i = 0; i < size; i++) {
            int index = i * 3 * 2;
            double x0 = myPoints.get(index);
            double z0 = myPoints.get(index + 1);
            double x1 = myPoints.get(index + 2);
            double z1 = myPoints.get(index + 3);
            double x2 = myPoints.get(index + 4);
            double z2 = myPoints.get(index + 5);
            double x3 = myPoints.get(index + 6);
            double z3 = myPoints.get(index + 7);

            gl.glBindTexture(GL.GL_TEXTURE_2D, myTexture.getTextureId());
            gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);
            //separate a Road to 100 traces depends on width,each trace using trangle tri
            for (int s = 0; s < TIMES; s++) {
                double X1 = x0;
                double Z1 = z0;
                gl.glBegin(GL2.GL_TRIANGLE_STRIP);
                gl.glNormal3d(0, 1, 0);
                double[] vInit = {
                        x1 - x0, z1 - z0, 0
                };
                double wEachSide = myWidth / 2.0;
                double[] vOth = MathUtil.normal(MathUtil.multiply(MathUtil.rotationMatrix(90),
                        vInit));
                
                double X = (vOth[0] * myWidth) / TIMES;
                double Z = (vOth[1] * myWidth) / TIMES;
                gl.glTexCoord2d(0,s/100.f);
                gl.glVertex3d(
                        X1 + vOth[0] * wEachSide -s*X,
                        Terrain.altitude(X1 + vOth[0] * wEachSide - s*X, Z1 + vOth[1] * wEachSide -s*Z) + 0.03,
                        Z1 + vOth[1] * wEachSide -s*Z);
                gl.glTexCoord2d(0,(s+1)/100.f);
                gl.glVertex3d(
                        X1 + vOth[0] * wEachSide - (s+1)*X,
                        Terrain.altitude(X1 + vOth[0] * wEachSide - (s+1)*X, Z1 + vOth[1] * wEachSide - (s+1)*Z) + 0.03,
                        Z1 + vOth[1] * wEachSide - (s+1)*Z);
                //sparate trace to 100 small part depends on length,using two trangles construct one small part
                for (double t = step; t < 1.0; t += step) {
                    double X2 = b(0, t) * x0 + b(1, t) * x1 + b(2, t) * x2 + b(3, t) * x3;
                    double Z2 = b(0, t) * z0 + b(1, t) * z1 + b(2, t) * z2 + b(3, t) * z3;
                    double[] v = {
                            X2 - X1, Z2 - Z1, 0
                    };
                    vOth = MathUtil.normal(MathUtil.multiply(MathUtil.rotationMatrix(90), v));
                    X = (2 * vOth[0] * wEachSide) / TIMES;
                    Z = (2 * vOth[1] * wEachSide) / TIMES;
                    gl.glTexCoord2d(t,s/100.f);
                    //vertexs' y position is calculated by Terrain altitude method
                    gl.glVertex3d(
                            X2 + vOth[0] * wEachSide - s*X,
                            Terrain.altitude(X2 + vOth[0] * wEachSide - s*X, Z2 + vOth[1] * wEachSide - s*Z) + 0.03,
                            Z2 + vOth[1] * wEachSide - s*Z);
                    gl.glTexCoord2d(t,(s+1)/100.f);
                    gl.glVertex3d(
                            X2 + vOth[0] * wEachSide - (s+1)*X,
                            Terrain.altitude(X2 + vOth[0] * wEachSide - (s+1)*X, Z2 + vOth[1] * wEachSide - (s+1)*Z) + 0.03,
                            Z2 + vOth[1] * wEachSide - (s+1)*Z);
                    X1 = X2;
                    Z1 = Z2;
                }
                double[] vEnd = {
                        x3 - x2, z3 - z2, 0
                };
                vOth = MathUtil.normal(MathUtil.multiply(MathUtil.rotationMatrix(90), vEnd));
                X = (2 * vOth[0] * wEachSide) / TIMES;
                Z = (2 * vOth[1] * wEachSide) / TIMES;
                gl.glTexCoord2d(1,s/100.f);
                gl.glVertex3d(
                        x3 + vOth[0] * wEachSide - s*X,
                        Terrain.altitude(x3 + vOth[0] * wEachSide - s*X, z3 + vOth[1] * wEachSide - s*Z) + 0.03,
                        z3 + vOth[1] * wEachSide - s*Z);
                gl.glTexCoord2d(1,(s+1)/100.f);
                gl.glVertex3d(
                        x3 + vOth[0] * wEachSide - (s+1)*X,
                        Terrain.altitude(x3 + vOth[0] * wEachSide - (s+1)*X, z3 + vOth[1] * wEachSide - (s+1)*Z) + 0.03,
                        z3 + vOth[1] * wEachSide - (s+1)*Z);
                gl.glEnd();
            }

        }
        gl.glPopMatrix();
        //myTexture.release(gl);
    }

    /**
     * Calculate the Bezier coefficients
     * 
     * @param i
     * @param t
     * @return
     */
    private double b(int i, double t) {

        switch (i) {

            case 0:
                return (1 - t) * (1 - t) * (1 - t);

            case 1:
                return 3 * (1 - t) * (1 - t) * t;

            case 2:
                return 3 * (1 - t) * t * t;

            case 3:
                return t * t * t;
        }

        // this should never happen
        throw new IllegalArgumentException("" + i);
    }

    public void init(GL2 gl) {
        myTexture = new MyTexture(gl, "ass2/spec/bricks.png", "png", true);
    }

}
