package ass2.spec;

public class MathUtil {
   
    public static double[] crossMultiply(double[] v1,double[] v2) {
        double[] retD = {
                v1[1]*v2[2] - v1[2]*v2[1],
                v1[2]*v2[0] - v1[0]*v2[2],
                v1[0]*v2[1] - v1[1]*v2[0]};
        
        return retD;
    }
    
    public static double[] normal(double[] v) {
        double len = Math.sqrt(v[0]*v[0] + v[1] * v[1] + v[2] * v[2]);
        double[] retD = {v[0]/len,v[1]/len,v[2]/len};
        return retD;
    }
    
    /**
     * Multiply a vector by a matrix
     * 
     * @param m A 3x3 matrix
     * @param v A 3x1 vector
     * @return
     */
    public static double[] multiply(double[][] m, double[] v) {

        double[] u = new double[3];

        for (int i = 0; i < 3; i++) {
            u[i] = 0;
            for (int j = 0; j < 3; j++) {
                u[i] += m[i][j] * v[j];
            }
        }

        return u;
    }
    /**
     * todo: A 2D rotation matrix for the given angle
     * 
     * @param angle
     * @return
     */
    public static double[][] rotationMatrix(double angle) {
        double[][] u = initializationMatrix(3);
        angle = normaliseAngle(angle);
        double s_a = Math.sin(angle * Math.PI / 180);
        double c_a = Math.cos(angle * Math.PI / 180);
        u[0][0] = c_a;
        u[0][1] = -s_a;
        u[1][0] = s_a;
        u[1][1] = c_a;
        u[2][2] = 1;
        return u;
    }
    
    static public double normaliseAngle(double angle) {
        return ((angle + 180.0) % 360.0 + 360.0) % 360.0 - 180.0;
    }
    
    public static double[][] initializationMatrix(int dimension) {
        double [][] u = new double[dimension][dimension];
        for(int i = 0;i < dimension;i ++)
            u[i][i] = 1.f;
        return u;
    }
}
