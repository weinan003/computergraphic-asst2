package ass2.spec;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

/**
 * COMMENT: Comment Tree 
 *
 * @author malcolmr
 */
public class Tree {

    private double[] myPos;
    private MyTexture myTexture;
    public Tree(double x, double y, double z) {
        myPos = new double[3];
        myPos[0] = x;
        myPos[1] = y;
        myPos[2] = z;
    }
    
    public double[] getPosition() {
        return myPos;
    }
    
    public void Draw(GL2 gl) {
        gl.glPushMatrix();
        gl.glTranslated(myPos[0], myPos[1], myPos[2]);
        drawTrunk(gl);
        gl.glTranslated(0, 0.6, 0);
        drawLeaf(gl);
        gl.glPopMatrix();
    }
    
    public void drawTrunk(GL2 gl) {
        //using a cylinder create trunk
        float[] diffuse = {(float) 0.505,(float) 0.411,(float) 0.08};
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, diffuse,0);
        gl.glBegin(GL2.GL_QUADS);
        {
            double radius= 0.1f;
            for(int i = 0;i < 64;i++) {
                double a = (i / 64.0)* Math.PI*2;
                double x0 = Math.cos(a) * radius;
                double z0 = Math.sin(a) * radius;
                a = (i+1/64.0)* Math.PI*2;
                double x1 = Math.cos(a) * radius;
                double z1 = Math.sin(a) * radius;
                double[] v1 = {x1-x0,0.0,0.0};
                double[] v2 = {0.0,0.0,z1-z0};
                double [] normal = MathUtil.normal(MathUtil.crossMultiply(v1, v2));
                gl.glNormal3d(normal[0], normal[1], normal[2]);
                gl.glVertex3d(x0, 0, z0);
                gl.glVertex3d(x1, 0, z0);
                gl.glVertex3d(x1, 0.8, z1);
                gl.glVertex3d(x0, 0.8, z1);
            }
        }
        gl.glEnd();
    }
    
    double r(double t){
        double x  = Math.cos(2 * Math.PI * t);
        return x;
    }
    
    double getY(double t){
        double y  = Math.sin(2 * Math.PI * t);
        return y;
    }
    
    public void drawLeaf(GL2 gl) {
        //using sphere creat leaves
        float[] diffuse = {1.f,1.f,1.f,1.f};
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, diffuse,0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, myTexture.getTextureId());
        gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);
        double deltaT;
        deltaT = 0.5/10;
        int ang;  
        int delang = 360/20;
        double x1,x2,z1,z2,y1,y2;
        double radius = 0.3;
        for (int i = 0; i < 10; i++) 
        { 
            double t = -0.25 + i*deltaT;
            
            gl.glBegin(GL2.GL_TRIANGLE_STRIP); 
            for(int j = 0; j <= 20; j++)  
            {  
                ang = j*delang;
                x1=radius * r(t)*Math.cos((double)ang*2.0*Math.PI/360.0); 
                x2=radius * r(t+deltaT)*Math.cos((double)ang*2.0*Math.PI/360.0); 
                y1 = radius * getY(t);

                z1=radius * r(t)*Math.sin((double)ang*2.0*Math.PI/360.0);  
                z2= radius * r(t+deltaT)*Math.sin((double)ang*2.0*Math.PI/360.0);  
                y2 = radius * getY(t+deltaT);

                double normal[] = {x1,y1,z1};


                normal = MathUtil.normal(normal);    

                gl.glNormal3dv(normal,0); 
                gl.glTexCoord2d(j/20.0, 0);
                gl.glVertex3d(x1,y1,z1);
                normal[0] = x2;
                normal[1] = y2;
                normal[2] = z2;

                normal = MathUtil.normal(normal);    
                gl.glNormal3dv(normal,0);
                gl.glTexCoord2d(j/20.0, 1);
                gl.glVertex3d(x2,y2,z2); 

            }; 
            gl.glEnd();  
        }
    }

    public void init(GL2 gl) {
         myTexture = new MyTexture(gl, "ass2/spec/grass.bmp", "bmp", true);
        
    }
}
